# Flag Rippler for Paint.NET

This was made to help with spriting for my [Starbound More World Flags submod](../../../../../MoofEMP/starbound-more-world-flags).

[`Flag Rippler.cs`](Flag%20Rippler.cs) is the source code  
[`Flag Rippler.dll`](Flag%20Rippler.dll) is the compiled .dll  
[`logo.png`](logo.png) is the effect icon  
[`template.png`](template.png) is the background this effect was designed to work with

**INSTALLATION INSTRUCTIONS:**  
You only need to download [`Flag Rippler.dll`](Flag%20Rippler.dll) and place it in `C:\Program Files\paint.net\Effects`

Created with CodeLab: https://boltbait.com/pdn/codelab/

Paint.NET: https://www.getpaint.net/

## Usage (you gotta follow this to the letter otherwise I dunno what'll happen):

Create an 18x12 flag reference; this is an example:  
<img src="samples/exampleflag.png" alt="exampleflag">

On a new layer, position it on top of the first flagpole in [`template.png`](template.png):  
<img src="samples/overlaid.png" alt="overlaid">

Run the Flag Rippler effect and reposition if needed:  
<img src="samples/effect.png" alt="effect">
